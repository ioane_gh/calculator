package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private var first_number = 0.0
    private var second_number = 0.0
    private var operation = ""
    private var bool = false
    private var isDot = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){
        button0.setOnClickListener(this)
        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)
    }

    fun dot(view: View){
        if (!isDot){
            button_dot.isClickable = false
        }
        isDot = true
    }

    fun divide(view: View){
        val value = result_textview.text.toString()
        if (value.isNotEmpty()) {
            operation = "/"
            first_number = value.toDouble()
            result_textview.text = ""
            bool = true
            button_dot.isClickable = true
        }

    }

    fun times(view: View){
        button_dot.isClickable = false
        val value = result_textview.text.toString()
        if (value.isNotEmpty()){
            operation = "x"
            first_number = value.toDouble()
            result_textview.text = ""
            button_dot.isClickable = true
        }
    }

    fun minus(view: View){
        button_dot.isClickable = false
        val value = result_textview.text.toString()
        if (value.isNotEmpty()){
            operation = "-"
            first_number = value.toDouble()
            result_textview.text = ""
            button_dot.isClickable = true
        }
    }

    fun plus(view: View){
        button_dot.isClickable = false
        val value = result_textview.text.toString()
        if (value.isNotEmpty()){
            operation = "+"
            first_number = value.toDouble()
            result_textview.text = ""
            button_dot.isClickable = true
        }
    }

    fun equal(view: View){
        var result = 0.0
        val value = result_textview.text.toString()
        if (value.isNotEmpty() && bool) {
            second_number = value.toDouble()
            bool = false
            if (operation == "/" && second_number != 0.0) {
                result = first_number / second_number
            }

            if (operation == "x" ){
                result = first_number * second_number
            }

            if (operation == "-"){
                result = first_number - second_number
            }

            if (operation == "+"){
                result = first_number + second_number
            }

            result_textview.text = result.toString()
        }
    }


    fun delete(view: View){
        val value = result_textview.text.toString()
        if (value.isNotEmpty()) {
            result_textview.text = value.substring(0, value.length - 1)
        }
    }

    override fun onClick(v: View?) {
        val button = v as Button
        result_textview.text = result_textview.text.toString() + button.text.toString()
    }

}